from collections import OrderedDict
from typing import Any
from typing import Dict
from typing import List

async def convert_s3_bucket_to_present(hub, ctx, bucket_name: str) -> Dict[str, Dict[str, Any]]:
    
    if not bucket_name:
        return {}
    
    resource = hub.tool.boto3.resource.create(ctx, "s3", "Bucket", bucket_name)
    bucket_describe = await hub.tool.boto3.resource.describe(resource)
    lifecycle_config = await hub.exec.boto3.client.s3.get_bucket_lifecycle_configuration(ctx=ctx, Bucket=bucket_name)
    bucket_present = dictionary_mapping(bucket_describe, s3_bucket_parameters)
    
    if lifecycle_config:
        bucket_present["lifecycle_configuration"] = convert_lifecycle_configuration_to_present(hub, config=lifecycle_config["ret"])

    return bucket_present

def convert_lifecycle_configuration_to_present(hub, config: dict) -> Dict[str, Dict[str, Any]]:
     res = {}

     if not config: 
         return res

     rules = config.get("Rules")
     
     if not rules:
         return res

     res['rules'] = []
     
     for rule in rules:
         res['rules'].append(map_raw_config_rule_to_sls(rule))
     
     return res

def convert_lifecycle_configuration_to_raw(hub, config: dict) -> Dict[str, Dict[str, Any]]:
    
     res = {}

     if not config:
         return res

     rules = config.get("rules")
     if not rules:
         return res

     res['Rules'] = []
     
     for rule in rules:
      res['Rules'].append(map_sls_config_rule_to_raw(rule))

     return res

def map_sls_config_rule_to_raw(rule: dict) -> Dict[str, Dict[str, Any]]:
    res = {}

    if not rule:
        return res
    
    for parameter_old_key, parameter_new_key in lifecycle_configuration_rule_sls_to_raw_mapping.items():
       if rule.get(parameter_old_key) is not None:
          if parameter_old_key == "expiration":
             res[parameter_new_key] = dictionary_mapping(rule.get(parameter_old_key), expiration_sls_to_raw_mapping)
          elif parameter_old_key == "abort_incomplete_multipart_upload":
             res[parameter_new_key] = dictionary_mapping(rule.get(parameter_old_key), abort_incomplete_multipart_upload_sls_to_raw_mapping)
          elif parameter_old_key == "noncurrent_version_expiration":
             res[parameter_new_key] = dictionary_mapping(rule.get(parameter_old_key), noncurrent_days_raw_to_sls_mapping)
          elif parameter_old_key == "filter":
              res[parameter_new_key] = dictionary_mapping(rule.get(parameter_old_key), filter_sls_to_raw_mapping)
          else:
              res[parameter_new_key] = rule.get(parameter_old_key)
                      
    return res

def map_raw_config_rule_to_sls(rule) -> Dict[str, Dict[str, Any]]:
    resource_translated = {}
    if not rule:
        return resource_translated

    for parameter_old_key, parameter_new_key in lifecycle_configuration_rule_raw_to_sls_mapping.items():
       if rule.get(parameter_old_key) is not None:
          if parameter_old_key == "Expiration":
             resource_translated[parameter_new_key] = dictionary_mapping(rule.get(parameter_old_key), expiration_raw_to_sls_mapping)
          elif parameter_old_key == "AbortIncompleteMultipartUpload":
             resource_translated[parameter_new_key] = dictionary_mapping(rule.get(parameter_old_key), abort_incomplete_multipart_upload_raw_to_sls_mapping) 
          elif parameter_old_key == "NoncurrentVersionExpiration":
             resource_translated[parameter_new_key] = dictionary_mapping(rule.get(parameter_old_key), noncurrent_days_sls_to_raw_mapping) 
          elif parameter_old_key == "Filter":
             resource_translated[parameter_new_key] = dictionary_mapping(rule.get(parameter_old_key), filter_raw_to_sls_mapping)
          else:
             resource_translated[parameter_new_key] = rule.get(parameter_old_key)
    
    return resource_translated


def generate_lifecycle_rules_mutation_list(hub, desired_rules_list :list, actual_rules_list :list) -> Dict[str, Dict[str, Any]]:
    
    if not desired_rules_list or not actual_rules_list: 
        return []

    desired_rules_ids = list(map(lambda rule : rule.get("ID"), desired_rules_list))
    desired_rules_with_ids_dict = dict(zip(desired_rules_ids, desired_rules_list))
    actual_rules_ids = list(map(lambda rule : rule.get("ID"), actual_rules_list))
    actual_rules_with_ids_dict = dict(zip(actual_rules_ids, actual_rules_list))

    desired_rules_ids_set = set(desired_rules_ids)
    actual_rules_ids_set = set(actual_rules_ids)

    intersection_desired_and_actual_ids_set = actual_rules_ids_set.intersection(desired_rules_ids) 
    desired_non_colliding_lifecycle_configuration_ids_set = desired_rules_ids_set - intersection_desired_and_actual_ids_set
    actual_non_colliding_lifecycle_configuration_ids_set = actual_rules_ids_set - intersection_desired_and_actual_ids_set

    colliding_rules_list = [] 

    for id in intersection_desired_and_actual_ids_set:
      # desired state configuration is over-riding the actual state in case of collision
      colliding_rules_list.append(merge(desired_rules_with_ids_dict.get(id), actual_rules_with_ids_dict.get(id)))
    
    desired_non_colliding_rules_list = []
    
    for id in desired_non_colliding_lifecycle_configuration_ids_set:
      desired_non_colliding_rules_list.append(desired_rules_with_ids_dict.get(id))
    
    actual_non_colliding_lifecycle_configuration_rules_list = []
    
    for id in actual_non_colliding_lifecycle_configuration_ids_set:
      actual_non_colliding_lifecycle_configuration_rules_list.append(actual_rules_with_ids_dict.get(id))

    return colliding_rules_list + desired_non_colliding_rules_list + actual_non_colliding_lifecycle_configuration_rules_list 

def merge(dict1, dict2):
    res = {}
    for k in set(dict1.keys()).union(dict2.keys()):
        if k in dict1 and k in dict2:
            if isinstance(dict1[k], dict) and isinstance(dict2[k], dict):
                 res[k] =  mergedicts(dict1[k], dict2[k])
            else:
                # Value from first dict overrides one in first and we move on.
                res[k] = dict1[k]
        elif k in dict1:
            res[k] = dict1[k]
        else:
            res[k] = dict2[k]

    return res

def convert_raw_s3_bucket_to_present(hub, item) -> Dict[str, Dict[str, Any]]:
  res = []

  if not item:
    return res

  for parameter_old_key, parameter_new_key in s3_bucket_parameters.items():
            if item.get(parameter_old_key) is not None:
                res.append({parameter_new_key : item.get(parameter_old_key)})

  return res

def dictionary_mapping(item, mappingDict) -> Dict[str, Dict[str, Any]]:

  res = {}

  if not item or not mappingDict:
    return res

  for parameter_old_key, parameter_new_key in mappingDict.items():
            if item.get(parameter_old_key) is not None:
                 res[parameter_new_key] = item.get(parameter_old_key)

  return res

lifecycle_configuration_rule_raw_to_sls_mapping = OrderedDict(
        {
            "Expiration": "expiration",
            "ID": "id",
            "Filter": "filter",
            "Status": "status",
            "AbortIncompleteMultipartUpload" : "abort_incomplete_multipart_upload",
            "NoncurrentVersionExpiration" : "noncurrent_version_expiration"
        }
    )

lifecycle_configuration_rule_sls_to_raw_mapping = {value:key for key, value in lifecycle_configuration_rule_raw_to_sls_mapping.items()} 

filter_raw_to_sls_mapping = OrderedDict(
         {
                "Prefix": "prefix"
         })
         
filter_sls_to_raw_mapping = {value:key for key, value in filter_raw_to_sls_mapping.items()}

expiration_raw_to_sls_mapping = OrderedDict(
         {
                "Days" : "days"
         }
  )

expiration_sls_to_raw_mapping = {value:key for key, value in expiration_raw_to_sls_mapping.items()}

abort_incomplete_multipart_upload_raw_to_sls_mapping = OrderedDict(
         {
                "DaysAfterInitiation": "days_after_initiation"
         })

abort_incomplete_multipart_upload_sls_to_raw_mapping = {value:key for key, value in abort_incomplete_multipart_upload_raw_to_sls_mapping.items()}
         
noncurrent_days_sls_to_raw_mapping = OrderedDict(
         {
                "NoncurrentDays" : "noncurrent_days"
         }
  )

noncurrent_days_raw_to_sls_mapping = {value:key for key, value in noncurrent_days_sls_to_raw_mapping.items()} 

s3_bucket_parameters = OrderedDict(
            {
                "Name": "name",
                "CreationDate": "creation_date",
            }
        )


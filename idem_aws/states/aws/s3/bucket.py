"""
Autogenerated state module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__

"""
from collections import OrderedDict
from typing import Any
from typing import Dict
from deepdiff import DeepDiff

__contracts__ = ["resource"]


async def present(
    hub,
    ctx,
    name: str,
    acl: str = None,
    create_bucket_configuration: dict = None,
    grant_full_control: str = None,
    grant_read: str = None,
    grant_read_acp: str = None,
    grant_write: str = None,
    grant_write_acp: str = None,
    object_lock_enabled_for_bucket: bool = None,
    object_ownership: str = None,
    lifecycle_configuration: Dict = None
) -> Dict[str, Any]:
    r"""
    **Autogenerated function**
        ACL (string) -- The canned ACL to apply to the bucket.

        name (string) --
        [REQUIRED]

        The name of the bucket to create.

        CreateBucketConfiguration (dict) --
            The configuration information for the bucket.

        LocationConstraint (string) --
            Specifies the Region where the bucket will be created.
            If you don't specify a Region, the bucket is created
            in the US East (N. Virginia) Region (us-east-1).

        GrantFullControl (string) --
            Allows grantee the read, write, read ACP, and write ACP permissions on the bucket.

        GrantRead (string) --
            Allows grantee to list the objects in the bucket.

        GrantReadACP (string) --
            Allows grantee to read the bucket ACL.

        GrantWrite (string) --
            Allows grantee to create new objects in the bucket.

        For the bucket and object owners of existing objects, also allows deletions and overwrites of those objects.

        GrantWriteACP (string) --
            Allows grantee to write the ACL for the applicable bucket.

        ObjectLockEnabledForBucket (boolean) --
            Specifies whether you want S3 Object Lock to be enabled for the new bucket.

        ObjectOwnership (string) --
            The container element for object ownership for a bucket's ownership controls.

        BucketOwnerPreferred -
            Objects uploaded to the bucket change ownership to the bucket owner if
            the objects are uploaded with the bucket-owner-full-control canned ACL.

        ObjectWriter -
            The uploading account will own the object if the object is uploaded
            with the bucket-owner-full-control canned ACL.

        BucketOwnerEnforced -
            Access control lists (ACLs) are disabled and no longer affect permissions.
            The bucket owner automatically owns and has full control over every object
            in the bucket. The bucket only accepts PUT requests that don't specify an
            ACL or bucket owner full control ACLs, such as the bucket-owner-full-control
            canned ACL or an equivalent form of this ACL expressed in the XML format.

        LifecycleConfiguration - 
            Specifies the lifecycle configuration for objects in an Amazon S3 bucket.


    Request Syntax:
        resource:
          aws.s3.bucket.present:
            - ACL: 'private'|'public-read'|'public-read-write'|'authenticated-read'
            - Bucket: string
            - CreateBucketConfiguration: {
                'af-south-1'|'ap-east-1'|'ap-northeast-1'|'ap-northeast-2'|
                'ap-northeast-3'|'ap-south-1'|'ap-southeast-1'|'ap-southeast-2'|'ca-central-1'|
                'cn-north-1'|'cn-northwest-1'|'EU'|'eu-central-1'|'eu-north-1'|'eu-south-1'|
                'eu-west-1'|'eu-west-2'|'eu-west-3'|'me-south-1'|'sa-east-1'|'us-east-2'|
                'us-gov-east-1'|'us-gov-west-1'|'us-west-1'|'us-west-2'}
            - GrantFullControl: string
            - GrantRead: string
            - GrantReadACP: string
            - GrantWrite: string
            - GrantWriteACP: string
            - ObjectLockEnabledForBucket: True|False
            - ObjectOwnership: BucketOwnerPreferred'|'ObjectWriter'|'BucketOwnerEnforced'

    Returns:
        Dict[str, Any]

    Examples:

        .. code-block:: sls

            test_bucket-bb7bb32e9533:
              aws.s3.bucket.present:
                - name: test_bucket-bb7bb32e9533
                - acl: 'private'
                - create_bucket_configuration: 'us-west-1'
                - object_lock_enabled_for_bucket: False
                - object_ownership: 'BucketOwnerEnforced'


    """
    result = dict(comment="", old_state=None, new_state=None, name=name, result=True)
    resource = hub.tool.boto3.resource.create(ctx, "s3", "Bucket", name)
    before = await hub.tool.aws.s3.conversion_utils.convert_s3_bucket_to_present(ctx, bucket_name=name)

    result["old_state"] = before

    if ctx.get("test", False):
        if before:
            result["comment"] = f"Would update aws.s3.bucket {name}"
            result["result"] = True
        else:
            result["comment"] = f"Would create aws.s3.bucket {name}"
            result["result"] = True
        return result
   
    is_mutated = False    

    if not before:
        # create case
        try:
            # Build out S3 bucket creation here
            update_ret = await hub.exec.boto3.client.s3.create_bucket(
                ctx,
                **{
                    "ACL": acl,
                    "Bucket": name,
                    "CreateBucketConfiguration": create_bucket_configuration,
                    "GrantFullControl": grant_full_control,
                    "GrantRead": grant_read,
                    "GrantReadACP": grant_read_acp,
                    "GrantWrite": grant_write,
                    "GrantWriteACP": grant_write_acp,
                    "ObjectLockEnabledForBucket": object_lock_enabled_for_bucket,
                    "ObjectOwnership": object_ownership
                },
            )
            if not update_ret["result"]:
                result["comment"] = update_ret["comment"]
                result["result"] = False
                return result
            
            # set lifecycel configuration
            if lifecycle_configuration:
                lifecycle_config_raw = hub.tool.aws.s3.conversion_utils.convert_lifecycle_configuration_to_raw(lifecycle_configuration)
                lifecycle_config_ret = await hub.exec.boto3.client.s3.put_bucket_lifecycle_configuration(ctx=ctx, Bucket=name, LifecycleConfiguration=lifecycle_config_raw)
            
                if not lifecycle_config_ret["result"]:
                    result["comment"] = result["comment"] if result["comment"] is not None else ""
                    result["comment"] = result["comment"] + " Lifecycle configuration is not applied to the s3 bucket with name: " + name 
                    return result
                else:
                    result["comment"] = result["comment"] if result["comment"] is not None else ""
                    result["comment"] = result["comment"] + " Lifecycle configuration has been successfully applied to the s3 bucket with name: " + name

        except hub.tool.boto3.exception.ClientError as e:
            result["comment"] = f"{e.__class__.__name__}: {e}"
            result["result"] = False
            return result
        result["comment"] = f"'Created {name}'"
    else:
        # mutation already existing s3 bucket if existing case
        result["comment"] = f"Resource {name} already exists."
      
        if lifecycle_configuration:
           bucket_name = before['name']

           try:
              actual_lifecycle_config = await hub.exec.boto3.client.s3.get_bucket_lifecycle_configuration(ctx=ctx, Bucket=bucket_name)
           except:
              result["comment"] = str(e)
              result["result"] = False
           else:
              if not actual_lifecycle_config or not actual_lifecycle_config["result"]:
                 #create lifecycle configuration to the cloud entity
                 lifecycle_config_raw = hub.tool.aws.s3.conversion_utils.convert_lifecycle_configuration_to_raw(lifecycle_configuration)
               
                 try:
                    lifecycle_config_create_ret = await hub.exec.boto3.client.s3.put_bucket_lifecycle_configuration(ctx=ctx, Bucket=name, LifecycleConfiguration=lifecycle_config_raw)
                 except:
                    result["comment"] = str(e)
                    result["result"] = False
                 
                 if not lifecycle_config_create_ret or not lifecycle_config_create_ret["result"]:
                     result ["comment"] = "Lifecycle configuration is not applied to the s3 bucket with name: " + bucket_name
                     is_mutated = False
                 else:
                     is_mutated = True
                     result ["comment"] = "Lifecycle configuration has been successfully applied to the s3 bucket with name: " + bucket_name

              else:  
                 # update case
                 lifecycle_config_desired_raw = hub.tool.aws.s3.conversion_utils.convert_lifecycle_configuration_to_raw(lifecycle_configuration)
                 actual_config_rules_list = actual_lifecycle_config.get("ret").get('Rules')
         
                 diff = DeepDiff(lifecycle_config_desired_raw.get('Rules'), actual_config_rules_list, ignore_order=True, ignore_order_func=ignore_order_func)
           
                 if not diff or len(diff) == 0:
                    is_mutated = False
                 elif len(diff) > 0:

                     try:
                        config_list_mutation_spec = hub.tool.aws.s3.conversion_utils.generate_lifecycle_rules_mutation_list(lifecycle_config_desired_raw.get('Rules'), actual_config_rules_list)
                        lifecycle_config_spec = {"Rules" : config_list_mutation_spec}
                        lifecycle_config_mutation = await hub.exec.boto3.client.s3.put_bucket_lifecycle_configuration(ctx=ctx, Bucket=bucket_name, LifecycleConfiguration=lifecycle_config_spec)

                        if not lifecycle_config_mutation["result"]:
                           is_mutated = False
                           result["comment"] = "Lifecycle configuration is not applied to the s3 bucket with name: " + bucket_name
                        else:
                           result["comment"] = "Lifecycle configuration has been successfully applied to the s3 bucket with name: " + bucket_name
                           is_mutated = True
                     except Exception as e:
                        result["comment"] = str(e)
                        result["result"] = False
                        
    if not before or is_mutated:
        try:
            after = await hub.tool.aws.s3.conversion_utils.convert_s3_bucket_to_present(ctx, bucket_name=name)
        except Exception as e:
            result["comment"] = str(e)
            result["result"] = False
        else:
            result["old_state"] = before
            result["new_state"] = after
    else:
        result["new_state"] = result["old_state"]
    return result


async def absent(hub, ctx, name: str) -> Dict[str, Any]:
    r"""
    **Autogenerated function**

    Deletes the specified s3 bucket.

    Args:
        name(Text): The name of the s3 bucket.

    Returns:
        Dict[str, Any]

    Examples:

        .. code-block:: sls

            bucket-5435423646-456464:
              aws.s3.bucket.absent:
                - name: value
    """

    result = dict(comment="", old_state=None, new_state=None, name=name, result=True)
    before = await hub.tool.aws.s3.conversion_utils.convert_s3_bucket_to_present(ctx, bucket_name=name)

    if not before:
        result["comment"] = f"'{name}' already absent"
    elif ctx.get("test", False):
        result["comment"] = f"Would delete aws.s3.bucket {name}"
        return result
    else:
        try:
            ret = await hub.exec.boto3.client.s3.delete_bucket(ctx, **{"Bucket": name})
            result["result"] = ret["result"]
            if not result["result"]:
                result["comment"] = ret["comment"]
                result["result"] = False
                return result
            result["comment"] = f"Deleted '{name}'"
        except hub.tool.boto3.exception.ClientError as e:
            result["comment"] = f"{e.__class__.__name__}: {e}"
        result["old_state"] = before

    return result

async def describe(hub, ctx) -> Dict[str, Dict[str, Any]]:
    """
    Obtain S3 buckets under the given context for any user.
    :param hub:
    :param ctx:
    :return:
    """
    result = {}
    ret = await hub.exec.boto3.client.s3.list_buckets(ctx)
    

    if not ret["result"]:
        hub.log.debug(f"Could not describe S3 buckets {ret['comment']}")
        return {}

    for bucket in ret["ret"]["Buckets"]:
        resource_id = bucket.get("Name")

        # s3 bucket translated to present (sls format)
        resource_translated = hub.tool.aws.s3.conversion_utils.convert_raw_s3_bucket_to_present(bucket)

        # get lifecycle config from AWS cloud
        lifecycle_config = await hub.exec.boto3.client.s3.get_bucket_lifecycle_configuration(ctx=ctx, Bucket=resource_id)

        # enrich s3 bucket with lifecycle config present (sls) state
        if lifecycle_config and lifecycle_config["result"]:
            lifecycle_config_present = hub.tool.aws.s3.conversion_utils.convert_lifecycle_configuration_to_present(config=lifecycle_config["ret"])
            resource_translated.append({"lifecycle_configuration" : lifecycle_config_present})

        result[resource_id] = {"aws.s3.bucket.present": resource_translated}
    
    return result

def ignore_order_func(level):
        return 'a' in level.path()
import uuid

import pytest


@pytest.mark.localstack(pro=True)
@pytest.mark.asyncio
async def test_account(hub, ctx, aws_organization, aws_organization_unit):
    expected_account_name = "idem-acc" + str(uuid.uuid4())
    expected_email = "idem-email" + str(uuid.uuid4())
    expected_role_name = "idem-aws-role" + str(uuid.uuid4())
    expected_iam_user_access_to_billing = "ALLOW"
    expected_parent_id = aws_organization_unit.get("resource_id", None)
    expected_tags = [{"Key": "org", "Value": "idem"}, {"Key": "env", "Value": "test"}]

    # create account
    present_ret = await hub.states.aws.organizations.account.present(
        ctx,
        name=expected_account_name,
        email=expected_email,
        account_name=expected_account_name,
        role_name=expected_role_name,
        iam_user_access_to_billing=expected_iam_user_access_to_billing,
        parent_id=expected_parent_id,
        tags=expected_tags,
    )

    new_state = present_ret.get("new_state")
    old_state = present_ret.get("old_state")

    assert new_state and not old_state

    assert (
        new_state["Name"] == expected_account_name
    ), "Expected and Actual account_name should be equal"
    assert (
        new_state["Email"] == expected_email
    ), "Expected and Actual email should be equal"
    assert new_state["Tags"] == expected_tags
    assert new_state["ParentId"] == expected_parent_id

    list_accounts = await hub.exec.boto3.client.organizations.list_accounts_for_parent(
        ctx, ParentId=expected_parent_id
    )

    accounts = list_accounts["ret"].get("Accounts")

    filtered = list(
        filter(
            lambda acc: acc["Id"] == new_state["Id"] and acc["Arn"] == new_state["Arn"],
            accounts,
        )
    )

    assert filtered
    assert filtered[0]["Id"] == new_state["Id"]
    assert filtered[0]["Arn"] == new_state["Arn"]
    assert filtered[0]["Name"] == new_state["Name"]
    assert filtered[0]["Email"] == new_state["Email"]
    assert filtered[0]["Status"] == new_state["Status"]

    # update account's parent id and tags

    list_roots_resp = await hub.exec.boto3.client.organizations.list_roots(ctx)

    destination_parent_id = list_roots_resp["ret"]["Roots"][0]["Id"]

    updated_tags = [{"Key": "org", "Value": "idem-aws"}, {"Key": "env", "Value": "dev"}]

    update_ret = await hub.states.aws.organizations.account.present(
        ctx,
        name=new_state["Id"],
        email=expected_email,
        account_name=expected_account_name,
        role_name=expected_role_name,
        iam_user_access_to_billing=expected_iam_user_access_to_billing,
        parent_id=destination_parent_id,
        tags=updated_tags,
    )

    await assert_account(
        hub,
        ctx,
        update_ret,
        expected_parent_id,
        expected_account_name,
        expected_email,
        updated_tags,
        destination_parent_id,
    )

    updated_tags_2 = [
        {"Key": "org", "Value": "idem-aws-acc"},
        {"Key": "env", "Value": "sandbox"},
    ]

    update_with_no_parent = await hub.states.aws.organizations.account.present(
        ctx,
        name=new_state["Id"],
        email=expected_email,
        account_name=expected_account_name,
        role_name=expected_role_name,
        iam_user_access_to_billing=expected_iam_user_access_to_billing,
        tags=updated_tags_2,
    )

    await assert_account(
        hub,
        ctx,
        update_with_no_parent,
        destination_parent_id,
        expected_account_name,
        expected_email,
        updated_tags_2,
        destination_parent_id,
    )

    # describe account

    describe_state = await hub.states.aws.organizations.account.describe(ctx)

    assert new_state["Id"] in describe_state
    state = describe_state.get(new_state["Id"]).get("aws.organizations.account.present")
    assert expected_account_name == state[0]["account_name"]
    assert expected_email == state[1]["email"]
    assert destination_parent_id == state[2]["parent_id"]
    assert updated_tags_2 == state[3]["tags"]

    # remove account from organization
    delete_ret = await hub.states.aws.organizations.account.absent(
        ctx, name=update_with_no_parent.get("new_state")["Id"]
    )

    assert not delete_ret.get("new_state"), delete_ret.get("old_state")
    describe_after_delete = (
        await hub.states.aws.organizations.organization_unit.describe(ctx)
    )
    assert update_with_no_parent.get("new_state")["Id"] not in describe_after_delete


async def assert_account(
    hub,
    ctx,
    update_ret,
    expected_parent_id,
    expected_account_name,
    expected_email,
    updated_tags,
    destination_parent_id,
):
    before_update_state = update_ret.get("old_state")
    after_update_state = update_ret.get("new_state")

    assert before_update_state and after_update_state

    assert before_update_state["ParentId"] == expected_parent_id

    assert (
        after_update_state["Name"] == expected_account_name
    ), "Expected and Actual account_name should be equal "
    assert (
        after_update_state["Email"] == expected_email
    ), "Expected and Actual email should be equal"

    assert after_update_state["Tags"] == updated_tags
    assert after_update_state["ParentId"] == destination_parent_id

    list_accounts_for_new_parent = (
        await hub.exec.boto3.client.organizations.list_accounts_for_parent(
            ctx, ParentId=destination_parent_id
        )
    )

    accounts = list_accounts_for_new_parent["ret"].get("Accounts")

    filtered_after_update = list(
        filter(
            lambda acc: acc["Id"] == after_update_state["Id"]
            and acc["Arn"] == after_update_state["Arn"],
            accounts,
        )
    )

    assert filtered_after_update
    assert filtered_after_update[0]["Id"] == after_update_state["Id"]
    assert filtered_after_update[0]["Arn"] == after_update_state["Arn"]

import uuid

import pytest


@pytest.mark.asyncio
@pytest.mark.localstack(pro=False)
async def test_dynamodb_table(hub, ctx):
    # Create dynamodb table
    table_temp_name = "idem-test-dynamodb-table" + str(uuid.uuid4())
    tags = [{"Key": "Name", "Value": table_temp_name}]
    attribute_definitions = [
        {
            "AttributeName": "Artist",
            "AttributeType": "S",
        },
        {
            "AttributeName": "SongTitle",
            "AttributeType": "S",
        },
    ]
    key_schema = [
        {
            "AttributeName": "Artist",
            "KeyType": "HASH",
        },
        {
            "AttributeName": "SongTitle",
            "KeyType": "RANGE",
        },
    ]
    provisioned_throughput = {
        "ReadCapacityUnits": 123,
        "WriteCapacityUnits": 123,
    }
    billing_mode = "PROVISIONED"
    stream_specification = {"StreamEnabled": True, "StreamViewType": "NEW_IMAGE"}
    sse_specification = {
        "Enabled": True,
        "SSEType": "KMS",
        "KMSMasterKeyId": "abx-1234-456",
    }
    ret = await hub.states.aws.dynamodb.table.present(
        ctx,
        name=table_temp_name,
        attribute_definitions=attribute_definitions,
        key_schema=key_schema,
        billing_mode=billing_mode,
        provisioned_throughput=provisioned_throughput,
        stream_specification=stream_specification,
        sse_specification=sse_specification,
        tags=tags,
    )

    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert table_temp_name == resource.get("TableName")
    assert resource.get("AttributeDefinitions") == attribute_definitions
    assert resource.get("KeySchema") == key_schema
    assert resource.get("Tags")
    assert resource.get("Tags")[0].get("Key") == tags[0].get("Key")

    assert resource.get("ProvisionedThroughput")
    assert resource.get("ProvisionedThroughput").get(
        "ReadCapacityUnits"
    ) == provisioned_throughput.get("ReadCapacityUnits")
    assert resource.get("ProvisionedThroughput").get(
        "WriteCapacityUnits"
    ) == provisioned_throughput.get("WriteCapacityUnits")

    assert resource.get("SSEDescription")
    assert resource.get("StreamSpecification")

    assert resource.get("SSEDescription").get("SSEType") == sse_specification["SSEType"]
    assert resource.get("SSEDescription").get("KMSMasterKeyArn")
    assert (
        resource.get("StreamSpecification").get("StreamViewType")
        == stream_specification["StreamViewType"]
    )
    assert (
        resource.get("StreamSpecification").get("StreamEnabled")
        == stream_specification["StreamEnabled"]
    )

    tags = [
        {"Key": "Name", "Value": table_temp_name},
        {"Key": "new_tag", "Value": "new_value"},
    ]
    provisioned_throughput = {
        "ReadCapacityUnits": 150,
        "WriteCapacityUnits": 150,
    }

    # update dynamoDB table
    ret = await hub.states.aws.dynamodb.table.present(
        ctx,
        name=table_temp_name,
        attribute_definitions=attribute_definitions,
        key_schema=key_schema,
        provisioned_throughput=provisioned_throughput,
        tags=tags,
    )

    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert resource.get("ProvisionedThroughput")
    assert resource.get("ProvisionedThroughput").get(
        "ReadCapacityUnits"
    ) == provisioned_throughput.get("ReadCapacityUnits")
    assert resource.get("ProvisionedThroughput").get(
        "WriteCapacityUnits"
    ) == provisioned_throughput.get("WriteCapacityUnits")
    assert resource.get("Tags")
    assert resource.get("Tags")[0].get("Key") == tags[0].get("Key")
    assert resource.get("Tags")[1].get("Key") == tags[1].get("Key")

    # update dynamoDB table. This time with no tags in update. It should return old tags only.
    ret = await hub.states.aws.dynamodb.table.present(
        ctx,
        name=table_temp_name,
        attribute_definitions=attribute_definitions,
        key_schema=key_schema,
        provisioned_throughput=provisioned_throughput,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    new_resource = ret.get("new_state")
    old_resource = ret.get("old_state")
    assert new_resource.get("Tags") == old_resource.get("Tags")
    assert old_resource.get("Tags")[0].get("Key") == new_resource.get("Tags")[0].get(
        "Key"
    )
    assert old_resource.get("Tags")[1].get("Key") == new_resource.get("Tags")[1].get(
        "Key"
    )

    # Describe dynamodb_table
    describe_ret = await hub.states.aws.dynamodb.table.describe(ctx)
    assert describe_ret[table_temp_name]
    hub.tool.utils.verify_in_list(
        describe_ret[table_temp_name]["aws.dynamodb.table.present"], "tags", tags
    )
    assert describe_ret[table_temp_name].get("provisioned_throughput") is None

    # Delete dynamodb_table
    ret = await hub.states.aws.dynamodb.table.absent(ctx, name=table_temp_name)
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")

    # Trying to delete an already deleted resource. It should say resource already got deleted.
    ret = await hub.states.aws.dynamodb.table.absent(ctx, name=table_temp_name)
    assert ret["comment"] == f"'{table_temp_name}' already absent"

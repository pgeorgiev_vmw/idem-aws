import copy
import json
import uuid
from collections import ChainMap

import pytest


@pytest.mark.asyncio
async def test_user_policy(hub, ctx, aws_iam_user, aws_iam_user_2):
    policy_name = "idem-test-user-policy-" + str(uuid.uuid4())
    user_name = aws_iam_user.get("resource_id")
    policy_document = '{"Version": "2012-10-17","Statement":  {"Effect": "Allow", "Action": ["ec2:DescribeTags"], "Resource": "*"}}'

    # Create IAM user policy with test flag
    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True
    ret = await hub.states.aws.iam.user_policy.present(
        test_ctx,
        name=policy_name,
        user_name=user_name,
        policy_document=policy_document,
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert policy_name == resource.get("name")
    assert (
        f"Would create aws.iam.user_policy '{policy_name}' for user '{user_name}'"
        in ret["comment"][0]
    )
    assert resource.get("resource_id", None) is None, "resource_id not expected"

    resource_id_1 = f"{user_name}-{policy_name}"

    # Create IAM user policy
    ret = await hub.states.aws.iam.user_policy.present(
        ctx,
        name=policy_name,
        user_name=user_name,
        policy_document=policy_document,
    )
    assert ret["result"], ret["comment"]
    assert not ret["changes"].get("old") and ret["changes"]["new"]
    assert (
        f"Created aws.iam.user_policy '{policy_name}' for user '{user_name}'"
        in ret["comment"]
    )
    resource = ret.get("new_state")
    assert policy_name == resource.get("name")
    policy_name = resource.get("name")
    resource_id = resource.get("resource_id")
    assert resource_id_1 == resource_id, "resource_id expected"

    # Create IAM user policy - same policy different user
    user_name_2 = aws_iam_user_2.get("resource_id")
    resource_id_2 = f"{user_name_2}-{policy_name}"

    ret = await hub.states.aws.iam.user_policy.present(
        ctx,
        name=policy_name,
        user_name=user_name_2,
        policy_document=policy_document,
    )
    assert ret["result"], ret["comment"]
    assert not ret["changes"].get("old") and ret["changes"]["new"]
    assert (
        f"Created aws.iam.user_policy '{policy_name}' for user '{user_name_2}'"
        in ret["comment"]
    )
    resource = ret.get("new_state")
    assert policy_name == resource.get("name")
    policy_name = resource.get("name")
    resource_id = resource.get("resource_id")
    assert resource_id_2 == resource_id, "resource_id expected"

    # Describe IAM user policy -> should have the two user policies, even though the policy name is the same
    describe_ret = await hub.states.aws.iam.user_policy.describe(ctx)
    assert resource_id_1 in describe_ret
    assert resource_id_2 in describe_ret

    described_resource = describe_ret.get(resource_id_1).get(
        "aws.iam.user_policy.present"
    )
    described_resource_map = dict(ChainMap(*described_resource))
    assert "resource_id" in described_resource_map
    assert resource_id_1 == described_resource_map.get("resource_id")
    assert "user_name" in described_resource_map
    assert user_name == described_resource_map.get("user_name")
    assert "name" in described_resource_map
    assert policy_name == described_resource_map.get("name")
    assert "policy_document" in described_resource_map
    formatted_policy_document = json.dumps(
        json.loads(policy_document), separators=(", ", ": "), sort_keys=True
    )
    assert formatted_policy_document == described_resource_map.get("policy_document")

    # Update IAM user policy with test flag
    new_policy_document = '{"Version": "2012-10-17","Statement":  {"Effect": "Deny", "Action": ["ec2:Describe*"], "Resource": "*"}}'
    update_ret = await hub.states.aws.iam.user_policy.present(
        test_ctx,
        name=policy_name,
        user_name=user_name,
        policy_document=new_policy_document,
    )

    assert update_ret["result"], update_ret["comment"]
    assert update_ret.get("old_state") and update_ret.get("new_state")
    resource = update_ret.get("new_state")
    assert policy_name == resource.get("name")
    assert (
        f"Would update aws.iam.user_policy '{policy_name}' for user '{user_name}'"
        in update_ret["comment"][0]
    )
    assert (
        policy_name == resource.get("resource_id").split(user_name + "-")[1]
    ), f"resource_id is {user_name}-{policy_name}, extracted policy_name should match actual policy_name"

    assert policy_document != new_policy_document, "Policy document should be updated"
    assert formatted_policy_document == update_ret.get("old_state").get(
        "policy_document"
    )
    assert json.dumps(
        json.loads(new_policy_document), separators=(", ", ": ")
    ) == resource.get("policy_document")
    assert update_ret.get("old_state").get("policy_document") != resource.get(
        "policy_document"
    ), "Policy document should be updated between old_state and new_state"

    # Update IAM user policy
    update_ret = await hub.states.aws.iam.user_policy.present(
        ctx,
        name=policy_name,
        user_name=user_name,
        policy_document=new_policy_document,
    )

    assert update_ret["result"], update_ret["comment"]
    assert update_ret.get("old_state") and update_ret.get("new_state")
    resource = update_ret.get("new_state")
    assert policy_name == resource.get("name")
    assert "Update" in update_ret["comment"][0]
    assert (
        policy_name == resource.get("resource_id").split(user_name + "-")[1]
    ), f"resource_id is {user_name}-{policy_name}, extracted policy_name should match actual policy_name"

    assert formatted_policy_document == update_ret.get("old_state").get(
        "policy_document"
    )
    assert json.dumps(
        json.loads(new_policy_document), separators=(", ", ": "), sort_keys=True
    ) == resource.get("policy_document")
    assert update_ret.get("old_state").get("policy_document") != resource.get(
        "policy_document"
    ), "Policy document should be updated between old_state and new_state"

    # Delete IAM user policy with test flag
    ret = await hub.states.aws.iam.user_policy.absent(
        test_ctx,
        name=policy_name,
        user_name=user_name,
        resource_id=resource_id_1,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")
    assert (
        f"Would delete aws.iam.user_policy '{policy_name}' for user '{user_name}'"
        in ret["comment"][0]
    ), ret["comment"]

    # Delete IAM user policy
    ret = await hub.states.aws.iam.user_policy.absent(
        ctx,
        name=policy_name,
        user_name=user_name,
        resource_id=resource_id_1,
    )
    assert ret["result"], ret["comment"]
    assert ret["old_state"] and not ret["new_state"]
    assert (
        f"Deleted aws.iam.user_policy '{policy_name}' for user '{user_name}'"
        in ret["comment"]
    )

    # Delete second IAM user policy
    ret = await hub.states.aws.iam.user_policy.absent(
        ctx,
        name=policy_name,
        user_name=user_name_2,
        resource_id=resource_id_2,
    )
    assert ret["result"], ret["comment"]
    assert ret["old_state"] and not ret["new_state"]
    assert (
        f"Deleted aws.iam.user_policy '{policy_name}' for user '{user_name_2}'"
        in ret["comment"]
    )

    # Delete IAM user policy again and verify its a no-op
    ret = await hub.states.aws.iam.user_policy.absent(
        ctx,
        name=policy_name,
        user_name=user_name,
        resource_id=resource_id_1,
    )
    assert ret["result"], ret["comment"]
    assert (not ret.get("old_state")) and (not ret.get("new_state"))
    assert (
        f"aws.iam.user_policy '{policy_name}' for user '{user_name}' already absent"
        in ret["comment"]
    )

    # Delete second IAM user policy again and verify its a no-op
    ret = await hub.states.aws.iam.user_policy.absent(
        ctx,
        name=policy_name,
        user_name=user_name_2,
        resource_id=resource_id_2,
    )
    assert ret["result"], ret["comment"]
    assert (not ret.get("old_state")) and (not ret.get("new_state"))
    assert (
        f"aws.iam.user_policy '{policy_name}' for user '{user_name_2}' already absent"
        in ret["comment"]
    )

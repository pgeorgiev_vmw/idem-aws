import copy
import uuid

import pytest


@pytest.mark.asyncio
async def test_role_policy(hub, ctx, aws_iam_role, aws_iam_role_2):
    role_policy_temp_name = "idem-test-role-policy-" + str(uuid.uuid4())
    role_name = aws_iam_role.get("name")
    policy_document = '{"Version": "2012-10-17", "Statement": [{"Effect": "Allow", "Action": ["ec2:DescribeTags"], "Resource": "*"}]}'

    # Create IAM role policy with test flag
    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True
    ret = await hub.states.aws.iam.role_policy.present(
        test_ctx,
        name=role_policy_temp_name,
        role_name=role_name,
        policy_document=policy_document,
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert role_policy_temp_name == resource.get("name")
    assert "Would create" in str(ret["comment"])
    assert resource.get("resource_id", None) is None, "resource_id not expected"

    # Create IAM role policy
    ret = await hub.states.aws.iam.role_policy.present(
        ctx,
        name=role_policy_temp_name,
        role_name=role_name,
        policy_document=policy_document,
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert role_policy_temp_name == resource.get("name")
    resource_id = resource.get("resource_id")
    assert resource_id is not None, "resource_id expected"

    # Create IAM role policy - different role same policy
    role_name_2 = aws_iam_role_2.get("name")
    ret = await hub.states.aws.iam.role_policy.present(
        ctx,
        name=role_policy_temp_name,
        role_name=role_name_2,
        policy_document=policy_document,
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    resource = ret.get("new_state")
    assert role_policy_temp_name == resource.get("name")
    resource_id_2 = resource.get("resource_id")
    assert resource_id_2 is not None, "resource_id expected"

    # Describe -> should have the two role policies, eventhough the policy name is the same
    describe_ret = await hub.states.aws.iam.role_policy.describe(ctx)
    assert resource_id in describe_ret
    assert resource_id_2 in describe_ret

    # Create again
    ret = await hub.states.aws.iam.role_policy.present(
        ctx,
        name=role_policy_temp_name,
        resource_id=resource_id,
        role_name=role_name,
        policy_document=policy_document,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    assert "already exists" in str(ret["comment"]), ret["comment"]

    # Delete IAM role policy with test flag
    ret = await hub.states.aws.iam.role_policy.absent(
        test_ctx,
        name=role_policy_temp_name,
        role_name=role_name,
        resource_id=resource_id,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")
    assert "Would delete" in str(ret["comment"]), ret["comment"]

    # Delete IAM role policy
    ret = await hub.states.aws.iam.role_policy.absent(
        ctx, name=role_policy_temp_name, role_name=role_name, resource_id=resource_id
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")

    # Delete second IAM role policy
    ret = await hub.states.aws.iam.role_policy.absent(
        ctx,
        name=role_policy_temp_name,
        role_name=role_name_2,
        resource_id=resource_id_2,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")

    # Delete IAM role policy again
    ret = await hub.states.aws.iam.role_policy.absent(
        ctx, name=role_policy_temp_name, role_name=role_name, resource_id=resource_id
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and not ret.get("new_state")
